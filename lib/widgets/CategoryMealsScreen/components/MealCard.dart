import 'package:flutter/material.dart';
import 'package:shopping_app/utils/SceenNames.dart';

import '../../../utils/MealAffordability.dart';
import '../../../utils/MealPreparationDifficulty.dart';
import 'MealCardImage.dart';
import 'MealCardInfoBox.dart';

class MealCard extends StatelessWidget {
  final String id;
  final String title;
  final String imageURL;
  final double duration;
  final MealPreparationDifficulty difficulty;
  final MealAffordability affordability;

  const MealCard({
    @required this.id,
    @required this.title,
    @required this.imageURL,
    @required this.duration,
    @required this.difficulty,
    @required this.affordability,
  });

  String get difficultyText {
    switch (difficulty) {
      case MealPreparationDifficulty.EASY:
        return 'Easy';
      case MealPreparationDifficulty.MEDIUM:
        return 'Medium';
      case MealPreparationDifficulty.HARD:
        return 'Hard';
      default:
        return 'Unknown';
    }
  }

  String get affordableText {
    switch (affordability) {
      case MealAffordability.CHEAP:
        return 'Cheap';
      case MealAffordability.PRICEY:
        return 'Pricey';
      case MealAffordability.LUXURIOUS:
        return 'Luxurious';
      default:
        return 'Unknown';
    }
  }

  void _selectMeal(
      {@required BuildContext context,
      @required String routeToNavigate,
      Map<String, String> args}) {
    Navigator.of(context).pushNamed(routeToNavigate, arguments: args);
  }

  @override
  Widget build(BuildContext context) {
    var _selectMealCallback = () => _selectMeal(
          context: context,
          routeToNavigate: ScreenNames.detailsMealScreen,
          args: {
            'id': this.id,
          },
        );

    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      elevation: 5,
      margin: const EdgeInsets.all(10),
      child: Container(
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: _selectMealCallback,
            borderRadius: BorderRadius.circular(15),
            radius: 50,
            splashColor: Color.fromRGBO(
                Theme.of(context).primaryColor.red,
                Theme.of(context).primaryColor.green,
                Theme.of(context).primaryColor.blue,
                0.2),
            child: Column(
              children: [
                MealCardImage(
                  imageURL: imageURL,
                  title: title,
                  callback: _selectMealCallback,
                ),
                Divider(
                  thickness: 3,
                  height: 3,
                  color: Color.fromRGBO(180, 180, 180, 0.2),
                ),
                MealCardInfoBox(
                  duration: duration,
                  difficultyText: difficultyText,
                  affordableText: affordableText,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
