import 'package:flutter/material.dart';

class MealCardImage extends StatelessWidget {
  const MealCardImage({
    Key key,
    @required this.imageURL,
    @required this.title,
    @required this.callback,
  }) : super(key: key);

  final String imageURL;
  final String title;
  final Function callback;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15),
            topRight: Radius.circular(15),
          ),
          child: Image.network(
            imageURL,
            height: 250,
            width: double.infinity,
            fit: BoxFit.cover,
          ),
        ),
        Container(
          padding: EdgeInsets.all(12),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.elliptical(25, 20),
            ),
            boxShadow: const [
              BoxShadow(
                color: Color.fromRGBO(180, 180, 180, 0.5),
                blurRadius: 3.0,
                spreadRadius: 0.1,
                offset: const Offset(-2.0, -2.0),
              ),
            ],
          ),
          child: Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 14,
              fontFamily: 'Raleway',
            ),
            softWrap: true,
            overflow: TextOverflow.fade,
          ),
        ),
        Positioned.fill(
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: callback,
              splashColor: Color.fromRGBO(
                  Theme.of(context).primaryColor.red,
                  Theme.of(context).primaryColor.green,
                  Theme.of(context).primaryColor.blue,
                  0.2),
              radius: 50,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15),
                topRight: Radius.circular(15),
              ),
              highlightColor: Colors.transparent,
            ),
          ),
        ),
      ],
      alignment: Alignment.bottomRight,
    );
  }
}
