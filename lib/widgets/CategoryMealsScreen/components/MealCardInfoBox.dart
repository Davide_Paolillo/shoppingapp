import 'package:flutter/material.dart';

class MealCardInfoBox extends StatelessWidget {
  const MealCardInfoBox({
    Key key,
    @required this.duration,
    @required this.difficultyText,
    @required this.affordableText,
  }) : super(key: key);

  final double duration;
  final String difficultyText;
  final String affordableText;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Row(
        children: [
          Row(
            children: [
              Icon(Icons.schedule),
              SizedBox(
                width: 6,
              ),
              Text(
                '$duration min',
                style: TextStyle(fontFamily: 'Raleway'),
              ),
            ],
          ),
          Row(
            children: [
              Icon(Icons.star_half),
              SizedBox(
                width: 6,
              ),
              Text(
                '$difficultyText',
                style: TextStyle(fontFamily: 'Raleway'),
              ),
            ],
          ),
          Row(
            children: [
              Icon(Icons.attach_money),
              SizedBox(
                width: 6,
              ),
              Text(
                '$affordableText',
                style: TextStyle(fontFamily: 'Raleway'),
              ),
            ],
          ),
        ],
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      ),
    );
  }
}
