import 'package:flutter/material.dart';
import 'package:shopping_app/widgets/CategoriesScreen/utils/DummyData.dart';
import 'package:shopping_app/widgets/CategoryMealsScreen/components/MealCard.dart';

import 'package:shopping_app/widgets/SliverGlowStyledAppBar/SliverGlowStyledAppBar.dart';

class CategoryMealsScreen extends StatefulWidget {
  @override
  _CategoryMealsScreenState createState() => _CategoryMealsScreenState();
}

class _CategoryMealsScreenState extends State<CategoryMealsScreen> {
  @override
  Widget build(BuildContext context) {
    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, String>;
    final categoryTitle = routeArgs['title'];
    final categoryId = routeArgs['id'];

    final displayMealsBasedOnId = DUMMY_MEALS
        .where((element) => element.categories.contains(categoryId))
        .toList();

    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverGlowStyledAppBar(title: categoryTitle),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (ctx, index) => MealCard(
                id: displayMealsBasedOnId[index].id,
                title: displayMealsBasedOnId[index].title,
                difficulty: displayMealsBasedOnId[index].difficulty,
                affordability: displayMealsBasedOnId[index].affordability,
                duration: displayMealsBasedOnId[index].duration,
                imageURL: displayMealsBasedOnId[index].imageUrl,
              ),
              childCount: displayMealsBasedOnId.length,
            ),
          ),
        ],
      ),
    );
  }
}
