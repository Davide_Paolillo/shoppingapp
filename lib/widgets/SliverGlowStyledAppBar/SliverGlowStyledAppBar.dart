import 'package:flutter/material.dart';

import '../GlowStyledAppBar/GlowStyledAppBar.dart';

class SliverGlowStyledAppBar extends StatefulWidget {
  const SliverGlowStyledAppBar({@required this.title});

  final String title;

  @override
  _SliverGlowStyledAppBarState createState() => _SliverGlowStyledAppBarState();
}

class _SliverGlowStyledAppBarState extends State<SliverGlowStyledAppBar> {
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      expandedHeight: 110.0,
      collapsedHeight: 68.0,
      automaticallyImplyLeading: false,
      stretch: true,
      floating: true,
      elevation: 6,
      flexibleSpace: GlowStyledAppBar(
        title: widget.title,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          bottomRight: Radius.circular(20),
          bottomLeft: Radius.circular(20),
        ),
      ),
    );
  }
}
