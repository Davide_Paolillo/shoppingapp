import 'package:flutter/material.dart';

import '../CategoriesScreen/utils/DummyData.dart';
import '../SliverGlowStyledAppBar/SliverGlowStyledAppBar.dart';
import 'package:shopping_app/models/Meal.dart';

class DetailsMealScreen extends StatelessWidget {
  Container buildSectionTitle(
      BuildContext context, String title, IconData icon) {
    return Container(
      child: Row(
        children: [
          icon != null ? Icon(icon) : Container(),
          icon != null
              ? SizedBox(
                  width: 8,
                )
              : Container(),
          Text(
            title,
            style: Theme.of(context).textTheme.title,
          ),
        ],
        mainAxisAlignment: MainAxisAlignment.center,
      ),
      margin: const EdgeInsets.symmetric(
        vertical: 20,
      ),
    );
  }

  Padding buildContainerForRecipe(
      {@required BuildContext context,
      @required Meal selectedMeal,
      @required Widget child}) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Material(
        elevation: 6,
        borderRadius: BorderRadius.circular(15),
        child: Container(
          height: MediaQuery.of(context).size.height / 4,
          padding: const EdgeInsets.symmetric(
            horizontal: 10,
          ),
          decoration: BoxDecoration(
            color: Colors.amber.shade100.withOpacity(0.3),
            border: Border.all(
              color: Colors.grey,
            ),
            borderRadius: BorderRadius.circular(15),
          ),
          child: child,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, String>;

    final String mealId = routeArgs['id'];
    final selectedMeal =
        DUMMY_MEALS.firstWhere((element) => element.id == mealId);

    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverGlowStyledAppBar(
            title: selectedMeal.title,
          ),
          SliverToBoxAdapter(
            child: Card(
              elevation: 6,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15),
              ),
              child: Column(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15),
                      bottomRight: Radius.circular(2),
                      bottomLeft: Radius.circular(2),
                    ),
                    child: Image.network(
                      selectedMeal.imageUrl,
                      height: 300,
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Divider(
                    thickness: 3,
                    height: 3,
                  ),
                  buildSectionTitle(context, "Ingredients", Icons.fastfood),
                  buildContainerForRecipe(
                    context: context,
                    selectedMeal: selectedMeal,
                    child: ListView.builder(
                      itemBuilder: (ctx, index) => Center(
                        child: Container(
                          child: Text("• " + selectedMeal.ingredients[index]),
                          margin: const EdgeInsets.only(
                            bottom: 5,
                          ),
                        ),
                      ),
                      padding: const EdgeInsets.only(
                        top: 24,
                      ),
                      itemCount: selectedMeal.ingredients.length,
                      shrinkWrap: true,
                    ),
                  ),
                  buildSectionTitle(context, "Steps", Icons.list),
                  buildContainerForRecipe(
                    context: context,
                    selectedMeal: selectedMeal,
                    child: ListView.builder(
                      itemBuilder: (ctx, index) => Container(
                        child: Column(
                          children: [
                            ListTile(
                              leading: CircleAvatar(
                                backgroundColor: Theme.of(context).primaryColor,
                                foregroundColor: Colors.white,
                                child: Text("# ${index + 1}"),
                              ),
                              title: Text(selectedMeal.steps[index]),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Divider(
                              height: 4,
                              thickness: 2,
                              endIndent: 120,
                              indent: 120,
                            ),
                          ],
                        ),
                        margin: EdgeInsets.only(
                          bottom: 10,
                        ),
                      ),
                      itemCount: selectedMeal.steps.length,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
