import 'package:flutter/material.dart';
import 'package:shopping_app/utils/SceenNames.dart';

class CategoryCard extends StatelessWidget {
  final String id;
  final String title;
  final Color backgroundColor;

  const CategoryCard({
    @required this.title,
    @required this.id,
    this.backgroundColor,
  });

  void selectCategory(
      {BuildContext context,
      String routeToNavigate,
      Map<String, String> argumentsToPass}) {
    Navigator.of(context)
        .pushNamed(routeToNavigate, arguments: argumentsToPass);
  }

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;

    return Container(
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          splashColor: Color.fromRGBO(
              primaryColor.red, primaryColor.green, primaryColor.blue, 0.2),
          highlightColor: Colors.transparent,
          onTap: () => selectCategory(
              context: context,
              routeToNavigate: ScreenNames.categoryMealsScreenRoute,
              argumentsToPass: {
                'id': id,
                'title': title,
              }),
          borderRadius: BorderRadius.circular(15),
          child: Padding(
            padding: const EdgeInsets.all(15),
            child: Text(
              title,
              style: Theme.of(context).textTheme.title,
            ),
          ),
        ),
      ),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            backgroundColor.withOpacity(0.4),
            backgroundColor.withOpacity(0.6),
            backgroundColor.withOpacity(0.8),
            backgroundColor,
          ],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
        borderRadius: BorderRadius.circular(15),
        boxShadow: const [
          BoxShadow(
            color: Color.fromRGBO(120, 120, 120, 1),
            blurRadius: 4.0,
            spreadRadius: 1.0,
            offset: const Offset(2.0, 2.0),
          ),
        ],
      ),
    );
  }
}
