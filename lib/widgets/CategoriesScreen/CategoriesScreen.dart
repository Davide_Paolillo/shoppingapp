import 'package:flutter/material.dart';

import 'package:shopping_app/widgets/SliverGlowStyledAppBar/SliverGlowStyledAppBar.dart';
import 'components/CategoryCard.dart';
import 'utils/DummyData.dart';

class CategoriesScreen extends StatefulWidget {
  @override
  _CategoriesScreenState createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverGlowStyledAppBar(title: "ShoppingApp"),
          SliverPadding(
            padding: const EdgeInsets.all(15),
            sliver: SliverGrid(
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 200,
                childAspectRatio: 3 / 2,
                crossAxisSpacing: 20,
                mainAxisSpacing: 20,
              ),
              delegate: SliverChildBuilderDelegate(
                (ctx, index) => CategoryCard(
                  id: DUMMY_CATEGORIES[index].id,
                  title: DUMMY_CATEGORIES[index].title,
                  backgroundColor: DUMMY_CATEGORIES[index].color,
                ),
                childCount: DUMMY_CATEGORIES.toList().length,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
