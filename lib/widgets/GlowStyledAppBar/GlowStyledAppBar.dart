import 'package:flutter/material.dart';
import 'package:flutter_glow/flutter_glow.dart';

class GlowStyledAppBar extends StatelessWidget implements PreferredSizeWidget {
  const GlowStyledAppBar({
    Key key,
    this.title = '',
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      child: GlowContainer(
        glowColor: Theme.of(context).primaryColor.withAlpha(200),
        spreadRadius: 0.3,
        blurRadius: 9,
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Theme.of(context).primaryColorLight,
                Theme.of(context).primaryColorDark,
                Theme.of(context).primaryColor,
              ],
              begin: Alignment.bottomRight,
              end: Alignment.topLeft,
            ),
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20),
            ),
          ),
          padding: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top,
          ),
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Row(
              children: [
                Navigator.of(context).canPop()
                    ? GestureDetector(
                        onTap: () => Navigator.of(context).pop(),
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.white,
                        ),
                      )
                    : Container(),
                Spacer(),
                Center(
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width - 80,
                    child: Center(
                      child: Text(
                        title,
                        maxLines: 1,
                        softWrap: false,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontFamily: 'Raleway',
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: 22,
                        ),
                      ),
                    ),
                  ),
                ),
                Spacer(),
              ],
            ),
          ),
        ),
      ),
      preferredSize: Size(MediaQuery.of(context).size.width, 100),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(100);
}
