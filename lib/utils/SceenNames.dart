class ScreenNames {
  static const String categoryScreenRoute = 'CategoriesScreen';
  static const String categoryMealsScreenRoute = 'CategoryMealsScreen';
  static const String detailsMealScreen = 'DetailsMealScreen';
}
