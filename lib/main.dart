import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shopping_app/widgets/SliverGlowStyledAppBar/SliverGlowStyledAppBar.dart';

import 'widgets/DetailsMealScreen/DetailsMealScreen.dart';
import 'utils/SceenNames.dart';
import 'widgets/CategoryMealsScreen/CategoryMealsScreen.dart';
import 'widgets/CategoriesScreen/CategoriesScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return MaterialApp(
      title: "Shopping App",
      theme: ThemeData(
        primaryColor: Colors.deepPurple,
        primaryColorLight: Colors.deepPurple.shade400,
        primaryColorDark: Colors.deepPurple.shade500,
        accentColor: Colors.amber,
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        fontFamily: 'RobotoCondensed',
        textTheme: ThemeData.light().textTheme.copyWith(
            body1: TextStyle(color: Color.fromRGBO(20, 51, 51, 1)),
            body2: TextStyle(color: Color.fromRGBO(20, 51, 51, 1)),
            title: TextStyle(
              fontSize: 20,
              fontFamily: 'Raleway',
              fontWeight: FontWeight.bold,
            )),
      ),
      initialRoute: ScreenNames.categoryScreenRoute,
      routes: {
        ScreenNames.categoryScreenRoute: (ctx) => CategoriesScreen(),
        ScreenNames.categoryMealsScreenRoute: (ctx) => CategoryMealsScreen(),
        ScreenNames.detailsMealScreen: (ctx) => DetailsMealScreen(),
      },
      /* Set the route to navigate when we try to
      acceed inside a unexistent route */
      onGenerateRoute: (settings) => MaterialPageRoute(
        builder: (ctx) => CategoriesScreen(),
      ),
      // If toutes and onGenerateRoute fails flutter runs that function
      onUnknownRoute: (settings) => MaterialPageRoute(
        builder: (ctx) => Scaffold(
          body: CustomScrollView(
            slivers: [
              SliverGlowStyledAppBar(title: "Error Page"),
              SliverPadding(
                padding: const EdgeInsets.all(50),
                sliver: SliverToBoxAdapter(
                  child: Center(
                    child: Text("Error 404, we couldn't find that page!"),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
